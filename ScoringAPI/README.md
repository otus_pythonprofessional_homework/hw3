# Scoring API (ДЗ-3.0)

### Изменения

- Замена библиотеки `optparse` (Deprecated since version 3.2) на `argparse`
- Для статусов ответов используется `http.HTTPStatus`

### Тесты (unittest)

Запуск тестов
```
python3 -m unittest
or
python3 test.py
```
